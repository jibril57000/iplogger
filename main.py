from flask import request,Flask, redirect, render_template
import sqlite3 as sq
import datetime,token,socket

#Flask
app = Flask(__name__)
#Création du fichier des logs en mode append
with sq.connect("conn.sqlite3") as con:
    #On crée un curseur
    c = con.cursor()
    #On crée les tables
    c.execute("CREATE TABLE conn (id PRIMARY KEY NOT NULL, ip text, date text) ")
    c.execute("CREATE TABLE link (id PRIMARY KEY NOT NULL, path text, redirect text) ")
    #On actualise
    con.commit()

"""
    Route: /<route>/
    enregistre l'adresse ip du visiteur dans la base de données
    méthode post et get
"""
@app.route("/<route>/", methods=["GET", "POST"])
def route(route):
    send_back = ""
    with sq.connect("conn.sqlite3") as con:
        c = con.cursor()
        c.execute("INSERT INTO conn VALUES (NULL,?,?)", (request.remote_addr,datetime.now()))
        c.execute(f"SELECT * FROM link WHERE path='{route}'")
        send_back = c.fetchall()[0][2]
    return redirect(send_back)

"""
    Route: /admin
    Panel d'admin 
    -> permet de crée de nouvelles routes 
    methode = GET && noauth
"""
@app.route("/admin", methods=["GET"])
def admin():
    conn = []
    with sq.connect("conn.sqlite3") as con:
        c = con.cursor()
        c.execute(f"SELECT * FROM conn")
        conn = c.fetchall()
    return render_template("admin.html", connect=conn)


#Utiliser ceci en dev et utiliser un serveur wsgi en prod sinon aie les dégats !!!!
app.run(socket.gethostbyname(socket.gethostname()), 9999)